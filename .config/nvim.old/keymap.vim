"Keymap
"Evil arrow keys are evil
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
noremap   <Up>     <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>

"Holding down Ctrl goes 4x faster
nnoremap <C-h> 4h
nnoremap <C-j> 4j
nnoremap <C-k> 4k
nnoremap <C-l> 4l
vnoremap <C-h> 4h
vnoremap <C-j> 4j
vnoremap <C-k> 4k
vnoremap <C-l> 4l

nnoremap H ^
nnoremap L $

"Remap :redraw! to Ctrl-L, as Ctrl-l is overridden above
nnoremap <C-L> :redraw!<CR>
inoremap <C-L> <C-o>:redraw!<CR>

let mapleader="," "Set leader key
let maplocalleader="\\" "Set local leader key

"Use Ctrl-c like escape - Now C-c completes abbreviations
"Also map fd
nnoremap <C-c> <Esc>
inoremap <C-c> <Esc>
vnoremap <C-c> <Esc>
nnoremap fd <Esc>
inoremap fd <Esc>
vnoremap fd <Esc>

"M-[ and M-] to change tab
nnoremap <M-[> :tabprevious<CR>
nnoremap <M-]> :tabnext<CR>
"M-= for new tab
nnoremap <M-=> :tabnew<CR>
"M-- to close the tab
nnoremap <M--> :tabclose<CR>
"M-{ and M-} to rearrange tabs
nnoremap <M-{> :tabmove -1<CR>
nnoremap <M-}> :tabmove +1<CR>

nnoremap <leader>ne :NERDTreeTabsToggle<CR>
nnoremap <leader>nn :NERDTreeFocus<CR>
nnoremap <leader>ta :TagbarToggle<CR>
nnoremap <leader>u :UndotreeShow<bar>:UndotreeFocus<CR>
nnoremap <M-CR> :YcmCompleter FixIt<CR>
inoremap <M-CR> <C-o>:YcmCompleter FixIt<CR>
"Get Docs
nnoremap <leader>gd :YcmCompleter GetDoc<CR>

"Highlight Colors - Plugin: Colorizer
nnoremap <leader>hc :ColorToggle<CR>

"Markdown-preview
nnoremap <leader>pmm :MarkdownPreview<CR>
nnoremap <leader>psm :MarkdownPreviewStop<CR>

"Space to toggle folding
nnoremap <space> za

"- and _ to move a line up and down
nnoremap - ddp
nnoremap _ dd2kp

"Use Ctrl-[hjkl] to select the active split
"nnoremap <silent> <C-k> :wincmd k<CR>
"nnoremap <silent> <C-j> :wincmd j<CR>
"nnoremap <silent> <C-h> :wincmd h<CR>
"nnoremap <silent> <C-l> :wincmd l<CR>

"Use Meta-[hjkl] to resize
nnoremap <silent> <M-k> :resize +1<CR>
nnoremap <silent> <M-j> :resize -1<CR>
nnoremap <silent> <M-h> :vertical resize -1<CR>
nnoremap <silent> <M-l> :vertical resize +1<CR>

"Don't use Ex mode, use Q for formatting
noremap Q gq

"CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
"so that you can undo CTRL-U after inserting a line break.
inoremap <C-u> <C-g>u<C-u>

"Close window only if it won't quit vim
nnoremap <leader>q :close<CR>
nnoremap <leader>wq :w<BAR>close<CR>

"Build and run
"Build Build - Saves all then builds
nnoremap <leader>bb :wa<CR>:Dispatch<CR>

"Build CMake - Saves all, runs CMake, and reloads YCM
nnoremap <leader>bc :wa<CR>:!cd build && cmake ..<CR>:YcmCompleter ClearCompilationFlagCache<CR>:YcmForceCompileAndDiagnostics<CR>

"nvim-completion-manager
inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

"LanguageClient-neovim
nnoremap K :call LanguageClient_textDocument_hover()<CR>
nnoremap gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <F2> :call LanguageClient_textDocument_rename()<CR>

"Denite menus
nnoremap ,.b :Denite buffer<CR>
nnoremap ,.o :Denite outline<CR>
nnoremap ,.r :Denite register<CR>

"vim-multiple-cursors
let g:multi_cursor_start_key = '<M-n>'
let g:multi_cursor_select_all_key = 'g<M-n>'

"Edit and reload (source) Vim configs
"Edit Vim Vim
nnoremap <leader>evv :vsplit $HOME/.config/nvim<CR>
"Edit Vim Init
nnoremap <leader>evi :vsplit $HOME/.config/nvim/init.vim<CR>
"Edit Vim Config
nnoremap <leader>evc :vsplit $HOME/.config/nvim/config/vim.vim<CR>
"Edit Vim Keymap
nnoremap <leader>evk :vsplit $HOME/.config/nvim/keymap.vim<CR>
"Edit Vim colorScheme
nnoremap <leader>evs :vsplit $HOME/.config/nvim/colors.vim<CR>
"Reload Vim Vim
nnoremap <leader>rvv :source $MYVIMRC<CR>
"Reload Vim & Plugins
nnoremap <leader>rvp :source $MYVIMRC\|PlugInstall<CR>

