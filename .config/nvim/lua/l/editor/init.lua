--- User-specific editor tweaks
-- @module l.editor

local plug = require("c.plug")
local keybind = require("c.keybind")
local edit_mode = require("c.edit_mode")
local autocmd = require("c.autocmd")

local layer = {}

-- The startup buffer doesn't seem to pick up on vim.o changes >.<
local function set_default_buf_opt(name, value)
  vim.o[name] = value
  autocmd.bind_vim_enter(function()
    vim.bo[name] = value
  end)
end

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("sheerun/vim-polyglot") -- A bunch of languages
  plug.add_plugin("tpope/vim-surround") -- Awesome for dealing with surrounding things, like parens
  plug.add_plugin("jiangmiao/auto-pairs") -- Auto insert matching parens/quotes/stuff
  plug.add_plugin("preservim/nerdcommenter") -- Commenting
end

--- Configures vim and plugins for this layer
function layer.init_config()
  -- Space for leader, comma for local leader
  vim.g.mapleader = " "
  vim.g.maplocalleader = ","

  -- Required for NERDCommenter
  vim.cmd("filetype plugin on")

  -- Save undo history
  set_default_buf_opt("undofile", true)

  -- Allow a .vimrc file in a project directory with safe commands
  vim.o.exrc = true
  vim.o.secure = true

  -- Use `fd` to exit insert/visual/select/terminal mode
  keybind.bind_command(edit_mode.INSERT, "fd", "<esc>", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "fd", "<esc>", { noremap = true })
  keybind.bind_command(edit_mode.TERMINAL, "fd", "<C-\\><C-n>", { noremap = true })

  -- More convenient tabs
  keybind.bind_command(edit_mode.NORMAL, "<M-=>", ":tabnew<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-->", ":tabclose<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-[>", ":tabprevious<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-]>", ":tabnext<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-{>", ":tabmove -1<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-}>", ":tabmove +1<CR>", { noremap = true })

  -- H/L to jump to the start/end of a line
  keybind.bind_command(edit_mode.NORMAL, "H", "^", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "H", "^", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "L", "$", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "L", "$", { noremap = true })

  -- Holding down Ctrl makes cursor movement 4x faster
  keybind.bind_command(edit_mode.NORMAL, "<C-h>", "4h", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<C-j>", "4j", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<C-k>", "4k", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<C-l>", "4l", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<C-h>", "4h", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<C-j>", "4j", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<C-k>", "4k", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<C-l>", "4l", { noremap = true })

  -- M-h/j/k/l to resize windows
  keybind.bind_command(edit_mode.NORMAL, "<M-h>", ":vertical resize -1<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-j>", ":resize -1<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-k>", ":resize +1<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-l>", ":vertical resize +1<CR>", { noremap = true })

  -- `,,` to insert an arrow `->`
  keybind.bind_command(edit_mode.INSERT, ",,", "->", { noremap = true })
  -- `,.` to insert a different arrow `=>`
  keybind.bind_command(edit_mode.INSERT, ",.", "=>", { noremap = true })

  -- Edit config, reload config, and update plugins
  keybind.set_group_name("<leader>fe", "Editor")
  keybind.bind_command(edit_mode.NORMAL, "<leader>fed", ":edit $HOME/.config/nvim<CR>", { noremap = true }, "Edit config")
  keybind.bind_command(edit_mode.NORMAL, "<leader>feR", ":source $MYVIMRC<CR>", { noremap = true }, "Reload config")
  keybind.bind_command(edit_mode.NORMAL, "<leader>feU", ":source $MYVIMRC|PlugUpgrade|PlugClean|PlugUpdate|source $MYVIMRC<CR>",
    {noremap = true}, "Install and update plugins")

  -- Buffer
  keybind.set_group_name("<leader>b", "Buffers")
  keybind.bind_command(edit_mode.NORMAL, "<leader>bn", ":enew<CR>", { noremap = true }, "New buffer")

  -- Default indentation rules
  set_default_buf_opt("tabstop", 4)
  set_default_buf_opt("softtabstop", 4)
  set_default_buf_opt("shiftwidth", 4)
  set_default_buf_opt("expandtab", true) -- Use spaces instead of tabs
  set_default_buf_opt("autoindent", true)
  set_default_buf_opt("smartindent", true)

  -- NERDCommenter config
  vim.g.NERDCreateDefaultMappings = 0 -- Don't add default keybinds
  vim.g.NERDSpaceDelims = 1 -- Add a space after the comment delimiter
  keybind.set_group_name("<leader>c", "Comments")
  keybind.bind_command(edit_mode.NORMAL, "<leader>cl", ":call NERDComment('n', 'toggle')<CR>", { noremap = true, silent = true }, "Toggle line comment")
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<leader>cl", ":call NERDComment('x', 'toggle')<CR>", { noremap = true, silent = true }, "Toggle line comment")
end

return layer
